#include <iostream>
#include <vector>
#include <sstream>
#include <dirent.h>
#include <algorithm>
#include <map>
#include <fstream>
#include <thread>
#include <chrono>
#include <future>

bool running = true;
bool use_multi_threading = false;
std::string folder;

/**
 * Mutates string into lowercased.
 *
 * @param string
 * @return lowercased string
 */
void toLower(std::string& string) {
    std::for_each(string.begin(), string.end(), [](char &c) {
        c = std::tolower(c);
    });
}

/**
 * Checks if character is not a letter
 *
 * @param char
 * @return is letter
 */
bool isNotLetter(int c) {
    return !isalpha(c);
}

/**
 * Converts TimePoint to ms
 *
 * @param timepoint
 * @return milliseconds
 */
template<typename TimePoint>
std::chrono::milliseconds to_ms(TimePoint tp) {
    return std::chrono::duration_cast<std::chrono::milliseconds>(tp);
}

/**
 * Merges 2 frequency maps into one.
 * Complexity O(N)
 *
 * @param first 1 map
 * @param first 2 map
 * @return result map
 */
std::map<std::string, int> merge_maps(std::map<std::string, int> first, std::map<std::string, int> second) {
    for (auto it = first.cbegin(); it != first.cend(); it++) {
        if (second.find(it->first) != second.end()) {
            second.at(it->first) += it->second;
        } else {
            second.insert({it->first, it->second});
        }
    }
    return second;
}

/**
 * Splits the string into the separate words.
 * Lowercasing and removing digits and punctuation
 *
 * @param string
 * @return list of words
 */
std::vector<std::string> split(std::string &string) {
    std::vector<std::string> list;
    std::stringstream ss(string);
    std::basic_string<char> temp;
    while (ss >> temp) {
        toLower(temp);
        temp.erase (std::remove_if (temp.begin (), temp.end (), isNotLetter), temp.end ());
        list.push_back(temp);
    }
    return list;
}

/**
 * Checks if a string end with provided string
 *
 * @param string string to check
 * @param end expected end
 * @return ends with string
 */
bool ends_with(std::string string, std::string ends) {
    if (ends.size() > string.size()) return false;
    if (string.substr(string.size() - ends.size(), string.size()) != ends) return false;
    return true;
}

/**
 * Arguments validation
 *
 * @param arguments list of arguments
 * @return arguments validity
 */
bool check_arguments(std::vector<std::string> arguments) {
    if (arguments[0] != "multi" && arguments[0] != "single") {
        std::cout << "Unrecognizable threading parametrs. You can type only 'single' and 'multi'" << std::endl;
        return false;
    }

    if (arguments[0] == "multi") use_multi_threading = true;
        else use_multi_threading = false;
    folder = arguments[1];
    return true;
}

/**
 * Returns filenams of evry file in the provided folder
 *
 * @param folder name
 * @return list of filenames
 */
std::vector<std::string> get_required_files_paths(std::string folder) {
    std::vector<std::string> files2;
    DIR *dir;
    struct dirent *ent;
    if ((dir = opendir((folder).c_str())) != nullptr) {
        while ((ent = readdir(dir)) != nullptr) {
            std::string string(ent->d_name);
            if (ends_with(string, ".txt")) {
                files2.push_back(ent->d_name);
            }
        }
        closedir(dir);
        return files2;
    } else {
        perror("Could not open directory");
        exit(1);
    }
}


/**
 * Computing the counts of words in each file from the input
 *
 * @param files list of file names
 * @return map of frequencies
 */
std::map<std::string, int> compute_in_one_thread(const std::vector<std::string> &files, int thread) {
    std::map<std::string, int> frequencies;

    std::map<std::string, int>::iterator it;
    std::ifstream inputFile;
    std::string string;

    for (std::string file_name: files) {
        //std::cout << file_name << std::endl;
        inputFile.open(folder + "/" + file_name);

        while (getline(inputFile, string)) {
            std::vector<std::string> words = split(string);
            for (std::string word: words) {
                if (word.empty()) continue;
                it = frequencies.find(word);
                if (it != frequencies.end())
                    frequencies.at(word) += 1;
                else
                    frequencies.insert({word, 1});

            }
        }
        inputFile.close();
    }
    return frequencies;
}


/**
 * Program separates data into 2 pieces and will process them in parallel.
 * Each thread will compute own Map, which will be merged in the end.
 *
 * @param files list of file names
 * @return map of frequencies
 */
std::map<std::string, int> compute_in_multiple_threads(const std::vector<std::string> &files) {
    std::size_t const half_size = files.size() / 2;
    std::vector<std::string> split_lo(files.begin(), files.begin() + half_size);
    std::vector<std::string> split_hi(files.begin() + half_size, files.end());

    auto future1 = std::async(compute_in_one_thread, split_lo, 1);
    auto future2 = std::async(compute_in_one_thread, split_hi, 2);

    std::map<std::string, int> map1 = future1.get();
    std::map<std::string, int> map2 = future2.get();

    return merge_maps(map1, map2);
}

/**
 * Program will proint out infi in case of a "--help" command.
 * Or will run computing using single- or multithreading depending on a comand.
 * Program will close in case of an "end" comand.
 *
 * @param command
 * @return
 */
void process_command(std::string& command) {
    std::ofstream outputFile;
    std::vector<std::string> arguments;
    outputFile.open("output.txt");

    if (command == "--help") {
        std::cout << "Program would run automatically, if required arguments were provided" << std::endl;
        std::cout
                << "To run the program manually, you should write a command, where you define threading and required files ('multi' is for MultiThreading, 'single' is for SingleThreading)"
                << std::endl;
        std::cout << "Example: 'multi input_files.txt'" << std::endl;

    } else if ((arguments = split(command)).size() == 2) {
        while (!check_arguments(arguments)) {
            std::getline(std::cin, command);
            arguments = split(command);
        };
        std::vector<std::string> files = get_required_files_paths(folder);

        std::map<std::string, int> map;


        auto start = std::chrono::high_resolution_clock::now();
        if (!use_multi_threading)  map = compute_in_one_thread(files, 0);
            else map = compute_in_multiple_threads(files);
        auto end = std::chrono::high_resolution_clock::now();
        std::cout << "Needed for counting " << to_ms(end - start).count() << " ms to finish.\n";

        for (auto it = map.cbegin(); it != map.cend(); it++) {
            outputFile << it->first << ": " << it->second << '\n';
            //std::cout << it->first << ": " << it->second << '\n';
        }

        outputFile.close();
        map.clear();

    } else if (command == "end") {
        running = false;
    } else {
        std::cout << "Unknown command. Use --help" << std::endl;
    }
}


/**
 * Getting a command from console
 *
 */
void parse_command_line() {
    std::string command;
    std::getline(std::cin, command);
    process_command(command);
}

/**
 * Main program loop
 * If required arguments were provided, they will be processed as though it was a commad.
 * Otherwise the program will wait for the command from the console
 *
 * @param argc number of arguments
 * @param argv arguments
 * @return
 */
int main(int argc, char *argv[]) {
    std::cout << "Hello, this is a light word counting program.\n" << std::endl;

    if (argc == 3) {
        std::string args = std::string(argv[1] + std::string(" ") + argv[2]);
        process_command(args);
    } else {
        std::cout << "You didn't provide required arguments. ( --help )" << std::endl;
        parse_command_line();
    }

    while (running) {
        std::cout << "Waiting for a new command" << std::endl;
        parse_command_line();
    }

    return 0;
}

