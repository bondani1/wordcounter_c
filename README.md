    Before 1st start of the application set “working directory” in app configurations to root project directory
    Make sure, that your project name is the same, as in the CMake file

1) Overall info

Word counter is a program specialized for, obviously, counting word frequencies in files.
The program takes files, reads them, then counts frequencies of every single word (case insensitively) and saves them to the output file ordered alphabetically.
The program can work in 2 ways: using single thread and multiple threads.

2) How does it works

The program takes 2 arguments:
-) For thread selection ( “single” and “multi” )
-) For selecting a folder, where required files are located

	Example: “single folder_name/inputs” or “multi dataset”.

If arguments were not provided, you will be asked to provide them manually via console. The program takes only “single” for single threaded functionality and “multi” for multi threaded functionality. In case of a spelling mistake, you can type a command as many times as you need.

Now differences between Single- and MutiThreading will be described:
-)Single
   The program reads sequentially files from the provided folder.
   The program will fill the frequency map during reading files
   In the end the map will be returned and exported to the output .txt file, which will be sorted alphabetically
-) Multi
   The program will split data into 2 pieces for 2 threads.
   Each thread will process data as though it is a normal program.
   Each will write to own frequency map, which will be merged in the end
   Merged map is returned and exported to the output .txt file

3) Performance

    Single
        Sequential processing of 9 files takes ≈ 40ms
    Multi
        Processing using 2 threads takes ≈25-30ms
        May be optimized:
            Optimizing merge function (sequential operation with complexity O(N)
            Writing to the same map (synchronizing work)
  
